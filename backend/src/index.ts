import * as express from 'express';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import { questions, handleResponse} from './survey';

const app = express();
const port = 8080; 
app.use(cors());
app.use(bodyParser.json());

// define a route handler for the default home page
app.get( "/survey", ( req, res ) => {
    res.json(questions);
} );

app.post('/survey', (req, res) => {
  const response = handleResponse(req.body);
  res.json(response);
})

// start the Express server
app.listen( port, () => {
    console.log( `server started at http://localhost:${ port }` );
} );