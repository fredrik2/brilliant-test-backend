export const questions = [
    { "questionKey": "Q1", "text": "Would you recommend this app to friend?" }, 
    { "questionKey": "Q2", "text": "Do you trust your leadership?" },
    { "questionKey": "Q3", "text": "How much do you like yourself as a colleague?" }, 
    { "questionKey": "Q4", "text": "Do you have fun in this app?" },
    { "questionKey": "Q5", "text": "How much do you want to submit?" }
]

const benchmark = [
    {Q1: 3, Q2: 4, Q3: 4, Q4: 3, Q5: 1},
    {Q1: 3, Q2: 4, Q3: 4, Q4: 4, Q5: 5},
    {Q1: 1, Q2: 2, Q3: 2, Q4: 1, Q5: 2},
    {Q1: 4, Q2: 5, Q3: 5, Q4: 4, Q5: 5},
    {Q1: 2, Q2: 3, Q3: 4, Q4: 4, Q5: 4},
]

export const handleResponse = (surveyData: SurveyData[]) => {
    const benchAvgs: number[] = [];
    let totalUserScore = 0;
    benchmark.forEach(data => {
       benchAvgs.push(Object.values(data).reduce((a, b) => a + b, 0)/questions.length);
    });
    const benchAvg = benchAvgs.reduce((a, b) => a + b, 0)/benchAvgs.length;

    totalUserScore = surveyData.map(d => parseInt(d.answer as string)).reduce((a, b) => a + b, 0);

    return {
        userAvg: totalUserScore/questions.length,
        benchAvg: Math.round(benchAvg * 100) / 100 
    };
}

class SurveyData {
    questionKey!: string;
    answer!: number | string;
}8